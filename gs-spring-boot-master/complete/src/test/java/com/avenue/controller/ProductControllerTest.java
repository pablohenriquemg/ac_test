package com.avenue.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.avenue.model.Product;
import com.avenue.payload.ProductRequest;
import com.avenue.payload.ProductResponse;
import com.avenue.service.ProductService;
import com.google.gson.Gson;

public class ProductControllerTest extends BaseController {

	private MockMvc mockMvc;

	@Mock
	private ProductService productService;

	@InjectMocks
	private ProductController productController;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(productController)
				.setHandlerExceptionResolvers(webMvcConfigurationSupport.handlerExceptionResolver()).build();
	}

	@Test
	public void testGetProducts() throws Exception {
		when(productService.findAll()).thenReturn(new ArrayList<>());
		mockMvc.perform(get("/products").contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk());
	}

	@Test
	public void testSaveProduct() throws Exception {
		when(productService.save(any(ProductRequest.class))).thenReturn(new Product());
		mockMvc.perform(post("/products").contentType(MediaType.APPLICATION_JSON_UTF8).content(productRequest()))
				.andExpect(status().isOk());
	}

	@Test
	public void testGetProductById() throws Exception {
		when(productService.findById(any(Long.class))).thenReturn(new ProductResponse());
		mockMvc.perform(get("/products/1").contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk());
	}

	@Test
	public void testDeleteProductById() throws Exception {
		when(productService.deleteById(any(Long.class))).thenReturn(new ProductResponse());
		mockMvc.perform(delete("/products/1").contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk());
	}

	@Test
	public void testUpdateProduct() throws Exception {
		when(productService.updateProduct(any(ProductRequest.class))).thenReturn(new ProductResponse());
		mockMvc.perform(put("/products").contentType(MediaType.APPLICATION_JSON_UTF8).content(productRequest()))
				.andExpect(status().isOk());
	}

	private String productRequest() {
		ProductRequest ir = new ProductRequest();
		ir.setDescription("product description");
		Gson gson = new Gson();
		return gson.toJson(ir);
	}
}
