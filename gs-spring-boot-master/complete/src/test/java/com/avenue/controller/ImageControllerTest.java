package com.avenue.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.avenue.model.Image;
import com.avenue.payload.ImageRequest;
import com.avenue.payload.ImageResponse;
import com.avenue.service.ImageService;
import com.google.gson.Gson;

public class ImageControllerTest extends BaseController {

	private MockMvc mockMvc;

	@Mock
	private ImageService imageService;

	@InjectMocks
	private ImageController imageController;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(imageController)
				.setHandlerExceptionResolvers(webMvcConfigurationSupport.handlerExceptionResolver()).build();
	}

	@Test
	public void testGetImages() throws Exception {
		when(imageService.findAll()).thenReturn(new ArrayList<>());
		mockMvc.perform(get("/images").contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk());
	}

	@Test
	public void testSaveImage() throws Exception {
		when(imageService.save(any(ImageRequest.class))).thenReturn(new Image());
		mockMvc.perform(post("/images").contentType(MediaType.APPLICATION_JSON_UTF8).content(imageRequest()))
				.andExpect(status().isOk());
	}

	@Test
	public void testGetImageById() throws Exception {
		when(imageService.findById(any(Long.class))).thenReturn(new ImageResponse());
		mockMvc.perform(get("/images/1").contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk());
	}

	@Test
	public void testDeleteImageById() throws Exception {
		when(imageService.deleteById(any(Long.class))).thenReturn(new ImageResponse());
		mockMvc.perform(delete("/images/1").contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk());
	}

	@Test
	public void testUpdateImage() throws Exception {
		when(imageService.updateImage(any(ImageRequest.class))).thenReturn(new ImageResponse());
		mockMvc.perform(put("/images").contentType(MediaType.APPLICATION_JSON_UTF8).content(imageRequest()))
				.andExpect(status().isOk());
	}

	private String imageRequest() {
		ImageRequest ir = new ImageRequest();
		ir.setUrl("www.google.com");
		Gson gson = new Gson();
		return gson.toJson(ir);
	}

}
