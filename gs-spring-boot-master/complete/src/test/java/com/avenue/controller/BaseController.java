package com.avenue.controller;

import org.junit.Before;
import org.springframework.context.support.StaticApplicationContext;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

public class BaseController {

	public final static String PATH_RESOURCES = "src/test/resources/";

	final WebMvcConfigurationSupport webMvcConfigurationSupport = new WebMvcConfigurationSupport();

	final StaticApplicationContext applicationContext = new StaticApplicationContext();

	@Before
	public void setUp() throws Exception {
		webMvcConfigurationSupport.setApplicationContext(applicationContext);
	}

}
