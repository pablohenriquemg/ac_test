package com.avenue.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.avenue.model.Product;
import com.avenue.payload.ProductRequest;
import com.avenue.payload.ProductResponse;
import com.avenue.repository.ImageRepository;
import com.avenue.repository.ProductRepository;
import com.avenue.service.impl.ProductServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceImplTest {

	@InjectMocks
	private ProductServiceImpl productServiceImpl;

	@Mock
	private ImageRepository imageRepository;

	@Mock
	private ProductRepository productRepository;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void findAllTest() {
		List<Product> list = new ArrayList<>();
		list.add(new Product());
		when(productRepository.findAll()).thenReturn(list);
		List<ProductResponse> result = productServiceImpl.findAll();
		assertFalse(result.isEmpty());
	}

	@Test
	public void saveTest() {
		when(productRepository.save(any(Product.class))).thenReturn(new Product());
		Product saved = productServiceImpl.save(new ProductRequest());
		assertNotNull(saved);
	}

	@Test
	public void findByIdTest() {
		when(productRepository.findById(any(Long.class))).thenReturn(new Product());
		ProductResponse product = productServiceImpl.findById(1l);
		assertNotNull(product);
	}

}
