package com.avenue.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.avenue.model.Image;
import com.avenue.payload.ImageRequest;
import com.avenue.payload.ImageResponse;
import com.avenue.repository.ImageRepository;
import com.avenue.repository.ProductRepository;
import com.avenue.service.impl.ImageServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class ImageServiceImplTest {

	@InjectMocks
	private ImageServiceImpl imageServiceImpl;

	@Mock
	private ImageRepository imageRepository;

	@Mock
	private ProductRepository productRepository;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void findAllTest() {
		List<Image> list = new ArrayList<>();
		list.add(new Image());
		when(imageRepository.findAll()).thenReturn(list);
		List<ImageResponse> result = imageServiceImpl.findAll();
		assertFalse(result.isEmpty());
	}

	@Test
	public void saveTest() {
		when(imageRepository.save(any(Image.class))).thenReturn(new Image());
		Image saved = imageServiceImpl.save(new ImageRequest());
		assertNotNull(saved);
	}

	@Test
	public void findByIdTest() {
		when(imageRepository.findById(any(Long.class))).thenReturn(new Image());
		ImageResponse image = imageServiceImpl.findById(1l);
		assertNotNull(image);
	}

}
