package com.avenue.service;

import java.util.List;

import com.avenue.model.Image;
import com.avenue.payload.ImageRequest;
import com.avenue.payload.ImageResponse;

public interface ImageService {

	Image save(ImageRequest entity);

	List<ImageResponse> findAll();

	ImageResponse findById(Long id);

	ImageResponse deleteById(Long id);

	ImageResponse updateImage(ImageRequest imageRequest);
}
