package com.avenue.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avenue.model.Image;
import com.avenue.model.Product;
import com.avenue.payload.ImageResponse;
import com.avenue.payload.ProductRequest;
import com.avenue.payload.ProductResponse;
import com.avenue.repository.ImageRepository;
import com.avenue.repository.ProductRepository;
import com.avenue.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private ImageRepository imageRepository;

	@Override
	public Product save(ProductRequest entity) {
		List<Image> images = new ArrayList<>();
		if (entity.getImageList() != null) {
			entity.getImageList().forEach(idImage -> {
				images.add(imageRepository.findById(idImage));
			});
		}
		Product p = new Product(null, entity.getName(), entity.getDescription(), images);
		return productRepository.save(p);
	}

	@Override
	public List<ProductResponse> findAll() {
		List<ProductResponse> result = new ArrayList<>();
		List<Product> list = productRepository.findAll();
		list.forEach(product -> {
			ProductResponse productResponse = new ProductResponse();
			productResponse.setDescription(product.getDescription());
			productResponse.setName(product.getName());
			productResponse.setId(product.getProduct_id());		
			List<ImageResponse> imageList = new ArrayList<>();
			product.getImages().forEach(img -> {
				ImageResponse image = new ImageResponse();
				image.setUrl(img.getUrl());
				image.setId(img.getImage_id());
				imageList.add(image);
			});
			productResponse.setImageList(imageList);
			result.add(productResponse);
		});
		return result;
	}

	@Override
	public ProductResponse findById(Long id) {
		Product p = productRepository.findById(id);
		List<ImageResponse> imageList = new ArrayList<>();
		p.getImages().forEach(img -> {
			ImageResponse image = new ImageResponse();
			image.setUrl(img.getUrl());
			imageList.add(image);
		});
		return new ProductResponse(p.getName(), p.getDescription(), imageList);
	}

	@Override
	public ProductResponse deleteById(Long id) {
		ProductResponse productDeleted = findById(id);
		productRepository.delete(productRepository.findById(id));
		return productDeleted;
	}

	@Override
	public ProductResponse updateProduct(ProductRequest productRequest) {
		Product product = productRepository.findById(productRequest.getId());
		product.setDescription(productRequest.getDescription());
		product.setName(productRequest.getName());
		productRepository.save(product);
		return findById(productRequest.getId());
	}

}
