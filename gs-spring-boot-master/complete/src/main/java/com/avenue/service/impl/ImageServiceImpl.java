package com.avenue.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avenue.model.Image;
import com.avenue.model.Product;
import com.avenue.payload.ImageRequest;
import com.avenue.payload.ImageResponse;
import com.avenue.repository.ImageRepository;
import com.avenue.repository.ProductRepository;
import com.avenue.service.ImageService;

@Service
public class ImageServiceImpl implements ImageService {

	@Autowired
	private ImageRepository imageRepository;

	@Autowired
	private ProductRepository productRepository;

	@Override
	public Image save(ImageRequest entity) {
		Product product = null;
		if (entity.getProductId() != null) {
			product = productRepository.findById(entity.getProductId());
		}
		Image image = new Image(null, "https://robohash.org/mashape", product);
		return imageRepository.save(image);
	}

	@Override
	public List<ImageResponse> findAll() {
		List<ImageResponse> list = new ArrayList<>();
		List<Image> images = imageRepository.findAll();
		images.forEach(img -> {
			ImageResponse imageResponse = new ImageResponse();
			imageResponse.setId(img.getImage_id());
			imageResponse.setUrl(img.getUrl());
			list.add(imageResponse);
		});

		return list;
	}

	@Override
	public ImageResponse findById(Long id) {
		Image image = imageRepository.findById(id);
		return new ImageResponse(image.getUrl());
	}

	@Override
	public ImageResponse deleteById(Long id) {
		ImageResponse img = findById(id);
		imageRepository.delete(imageRepository.findById(id));
		return img;
	}

	@Override
	public ImageResponse updateImage(ImageRequest imageRequest) {
		Image image = imageRepository.findById(imageRequest.getId());
		image.setUrl(imageRequest.getUrl());
		imageRepository.save(image);
		return findById(imageRequest.getId());
	}

}
