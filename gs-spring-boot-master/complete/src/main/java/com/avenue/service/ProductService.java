package com.avenue.service;

import java.util.List;

import com.avenue.model.Product;
import com.avenue.payload.ProductRequest;
import com.avenue.payload.ProductResponse;

public interface ProductService {

	Product save(ProductRequest entity);

	List<ProductResponse> findAll();

	ProductResponse findById(Long id);

	ProductResponse deleteById(Long id);

	ProductResponse updateProduct(ProductRequest productRequest);
}
