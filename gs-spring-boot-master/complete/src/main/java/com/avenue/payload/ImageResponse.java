package com.avenue.payload;

import java.io.Serializable;

public class ImageResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3322616947271969952L;

	private Long id;
	private String url;

	public ImageResponse(String url) {
		super();
		this.url = url;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ImageResponse() {
		super();
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
