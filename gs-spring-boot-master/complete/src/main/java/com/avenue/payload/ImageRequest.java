package com.avenue.payload;

import java.io.Serializable;

public class ImageRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2007642767505020105L;

	private Long productId;

	private Long id;

	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

}
