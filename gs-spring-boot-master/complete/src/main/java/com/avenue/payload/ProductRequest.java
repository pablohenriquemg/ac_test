package com.avenue.payload;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProductRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4422791982024730579L;

	private Long id;
	private String name;
	private String description;
	private List<Long> imageList = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Long> getImageList() {
		return imageList;
	}

	public void setImageList(List<Long> imageList) {
		this.imageList = imageList;
	}

}
