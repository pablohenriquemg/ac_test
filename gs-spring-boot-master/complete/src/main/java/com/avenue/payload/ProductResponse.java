package com.avenue.payload;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProductResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2075716346550052132L;

	private Long id;
	private String name;
	private String description;
	private List<ImageResponse> imageList = new ArrayList<>();

	public ProductResponse() {
		super();
	}

	public ProductResponse(String name, String description, List<ImageResponse> imageList) {
		super();
		this.name = name;
		this.description = description;
		this.imageList = imageList;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<ImageResponse> getImageList() {
		return imageList;
	}

	public void setImageList(List<ImageResponse> imageList) {
		this.imageList = imageList;
	}

}
