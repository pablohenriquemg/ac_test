package com.avenue.controller;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.avenue.model.Image;
import com.avenue.payload.ImageRequest;
import com.avenue.payload.ImageResponse;
import com.avenue.service.ImageService;

@RestController
@RequestMapping("/images")
public class ImageController {

	@Autowired
	private ImageService imageService;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResponseEntity<?> getImages() {
		List<ImageResponse> result = imageService.findAll();
		return new ResponseEntity(result, HttpStatus.OK);
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<?> saveImage(@NotNull @RequestBody ImageRequest image) {
		Image i = imageService.save(image);
		return new ResponseEntity<>(i, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getById(@PathVariable("id") Long id) {
		ImageResponse p = imageService.findById(id);
		return new ResponseEntity<>(p, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteById(@PathVariable("id") Long id) {
		ImageResponse p = imageService.deleteById(id);
		return new ResponseEntity<>(p, HttpStatus.OK);
	}
	
	@RequestMapping(value = "", method = RequestMethod.PUT)
	public ResponseEntity<?> updateProduct(@NotNull @RequestBody ImageRequest imageRequest) {
		ImageResponse i = imageService.updateImage(imageRequest);
		return new ResponseEntity<>(i, HttpStatus.OK);
	}
}
