package com.avenue.controller;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.avenue.model.Product;
import com.avenue.payload.ProductRequest;
import com.avenue.payload.ProductResponse;
import com.avenue.service.ProductService;

@RestController
@RequestMapping("/products")
public class ProductController {

	@Autowired
	private ProductService productService;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResponseEntity<?> getProducts() {
		List<ProductResponse> result = productService.findAll();
		return new ResponseEntity(result, HttpStatus.OK);
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<?> saveProduct(@NotNull @RequestBody ProductRequest product) {
		Product p = productService.save(product);
		return new ResponseEntity<>(p, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getById(@PathVariable("id") Long id) {
		ProductResponse p = productService.findById(id);
		return new ResponseEntity<>(p, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteById(@PathVariable("id") Long id) {
		ProductResponse p = productService.deleteById(id);
		return new ResponseEntity<>(p, HttpStatus.OK);
	}
	
	@RequestMapping(value = "", method = RequestMethod.PUT)
	public ResponseEntity<?> updateProduct(@NotNull @RequestBody ProductRequest product) {
		Product p = productService.save(product);
		return new ResponseEntity<>(p, HttpStatus.OK);
	}

}
