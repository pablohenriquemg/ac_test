package com.avenue.repository;

import java.util.List;

import org.springframework.data.repository.Repository;

import com.avenue.model.Image;

public interface ImageRepository extends Repository<Image, Long> {

	Image save(Image entity);

	List<Image> findAll();

	Image findById(Long id);

	void deleteById(Long id);

	void delete(Image entity);
}
