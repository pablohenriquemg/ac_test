package com.avenue.repository;

import java.util.List;

import org.springframework.data.repository.Repository;

import com.avenue.model.Product;

public interface ProductRepository extends Repository<Product, Long> {

	Product save(Product entity);

	List<Product> findAll();

	Product findById(Long id);

	void deleteById(Long id);

	void delete(Product entity);
}
