package com.avenue.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "image")
public class Image implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3856545635830364909L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name = "url")
	private String url;

	@ManyToOne
	@JoinColumn(name = "fk_product")
	private Product product;

	public Image() {
		super();
	}

	public Image(Long image_id, String url, Product product) {
		super();
		this.id = image_id;
		this.url = url;
		this.product = product;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getImage_id() {
		return id;
	}

	public void setImage_id(Long image_id) {
		this.id = image_id;
	}

}
