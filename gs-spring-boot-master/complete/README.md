## Technologies used

* Java 8 (Open JDK)
* Spring Boot
* h2database 
* Maven
* Gson
* Apanche Tomcat

## Layers developed

### business
Layer that contains the implementation of business rules.

### controller
Layer that exposes the services of the communication interface with the frontend.

### model
Persistence layer containing domains, entities and repositories.

### repository
Layer that interacts with the database

### payload
Layer containing request and response templates

## Maven build, run

How to build/run:

* **build** : `$mvn clean install`
* **run** : `$mvn spring-boot:run`
